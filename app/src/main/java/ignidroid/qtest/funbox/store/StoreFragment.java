package ignidroid.qtest.funbox.store;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;

import ignidroid.qtest.funbox.BaseFragment;
import ignidroid.qtest.funbox.R;
import ignidroid.qtest.funbox.model.Product;

/**
 * Магазин
 */
public class StoreFragment extends BaseFragment {

    public static final String ARG_PRODUCTS = "ARG_PRODUCTS";
    private ViewPager pager;
    private StorePagerAdapter adapter;

    private OnBuyListener onBuyCallback = product -> { };

    @Override
    public void update(ArrayList<Product> data) {
        setProducts(data);
    }

    public interface OnBuyListener {
        void onBuy(int position);
    }
    public StoreFragment setOnBuyListener(OnBuyListener callback) {
        if(callback != null) {
            onBuyCallback = callback;
        }/* else {
            onBuyCallback = product -> { };
        }*/
        return this;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.store, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        hideToolbar();
        pager = view.findViewById(R.id.store_pager);
        //
        view.findViewById(R.id.store_buy_button).setOnClickListener(v -> {
            if(adapter.products.size() == 0) {
                Toast.makeText(getContext(), "Нет товаров для покупки", Toast.LENGTH_SHORT).show();
            } else { //покупаем
                onBuyCallback.onBuy(adapter.getRealPosition(pager.getCurrentItem()));
            }
        });
        Bundle args = getArguments();
        if(args == null) return;
        setProducts(args.getParcelableArrayList(ARG_PRODUCTS));
    }

    public void setProducts(ArrayList<Product> products) {
        if (adapter == null) {
            adapter = new StorePagerAdapter(getChildFragmentManager(), products);
            pager.setAdapter(adapter);
        } else {
            adapter.changeProducts(products);
        }
    }

} //eoc
