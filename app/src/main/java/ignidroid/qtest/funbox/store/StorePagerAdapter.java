package ignidroid.qtest.funbox.store;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

import ignidroid.qtest.funbox.ProductPage;
import ignidroid.qtest.funbox.model.Product;

public class StorePagerAdapter extends FragmentStatePagerAdapter {

    List<Product> products = new ArrayList<>();
    int[] realPosition;

    public StorePagerAdapter(FragmentManager fm, List<Product> products) {
        super(fm);
        changeProducts(products);
    }

    public void changeProducts(List<Product> products) {
        this.products.clear();
        if (products == null) {
            notifyDataSetChanged();
            return;
        }
        int size = products.size();
        realPosition = new int[size];
        int k = 0;
        for (int i = 0; i < size; i++) {
            Product product = products.get(i);
            if (product.inStock()) {
                this.products.add(product);
                realPosition[k++] = i;
            }
        }
        notifyDataSetChanged();
    }

    public int getRealPosition(int position) {
        return realPosition[position];
    }

    @Override
    public Fragment getItem(int position) {
        return ProductPage.newInstance(products.get(position));
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        return products.size();
    }

}//eoc
