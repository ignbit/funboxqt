package ignidroid.qtest.funbox;

import android.os.Bundle;
import android.support.annotation.MenuRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;

import ignidroid.qtest.funbox.model.Product;

/**
 * Базовый фрагмент главного экрана приложения
 */
public abstract class BaseFragment extends Fragment {

    /**
     * Ссылка на активность приложения
     */
    protected AppActivity activity;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (AppActivity) getActivity();
    }

    /**
     * Ссылка на {@code Toolbar} приложения
     */
    public Toolbar getToolbar() {
        return (getActivity() != null)? ((AppActivity)getActivity()).getToolbar() : null;
    }

    /**
     * Прячем верхнее меню
     */
    public void hideToolbar() {
        getToolbar().setVisibility(View.GONE);
    }

    /**
     * Подготовка основного меню приложения
     * @param title заголовок
     * @param subtitle подзаголовок
     * @param menuResId идентифиактор ресурса меню, либо значение "-1" - без меню
     * @param back инициализация кнопки "назад"
     */
    public void prepareToolbar(String title,
                               String subtitle,
                               @MenuRes int menuResId,
                               boolean back)
    {
        Toolbar toolbar = getToolbar();
        if(toolbar == null) return;
        toolbar.setVisibility(View.VISIBLE);
        Menu menu = toolbar.getMenu();
        menu.clear(); //очистим меню
        //устанавливаем меню
        if(menuResId != -1) {
            toolbar.inflateMenu(menuResId);
            //устанавливаем обработчик меню
            toolbar.setOnMenuItemClickListener(item -> {
                menuItemClick(item);
                return true;
            });
        }
        //устанавливаем заголовок
        toolbar.setTitle(title);
        //устанавливаем подзаголовок
        toolbar.setSubtitle(subtitle);
        if(back) { //кнопка "назад"
            //устанавливаем иконку навигации
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
            //установим режим обратной навигации
            toolbar.setNavigationOnClickListener(v -> activity.onBackPressed());
        } else {
            toolbar.setNavigationIcon(null);
        }
    }

    /**
     * Обработчик нажатия меню,<br>
     * вызывается в {@link #prepareToolbar(String, String, int, boolean)}
     * @param item элемент меню
     */
    protected void menuItemClick(MenuItem item) { }

    /**
     * Обновление визуальной части фрагмента
     * @param data данные обновления
     */
    public abstract void update(ArrayList<Product> data);

}//eoc
