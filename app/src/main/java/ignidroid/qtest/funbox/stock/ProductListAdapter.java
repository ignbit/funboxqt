package ignidroid.qtest.funbox.stock;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import ignidroid.qtest.funbox.R;
import ignidroid.qtest.funbox.model.Product;

public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.ProductListViewHolder> {

    private ArrayList<Product> products;
    private Context context;

    private OnItemClickListener mClickListener;
    interface OnItemClickListener {
        void onItemClick(View itemView, int position);
    }
    public void setOnItemClickListener(OnItemClickListener listener) {
        mClickListener = listener;
    }

    public ProductListAdapter(Context context, ArrayList<Product> products) {
        this.products = products;
        this.context = context;
    }

    @NonNull
    @Override
    public ProductListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ProductListViewHolder(
                LayoutInflater.from(parent.getContext()).
                        inflate(R.layout.stock_list_item, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull ProductListViewHolder holder, int position) {
        Product product = products.get(position);
        holder.name.setText(product.getName());
        holder.pcs.setText(
                context.getString(R.string.stock_value, product.getStock())
        );
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    void changeProducts(ArrayList<Product> products) {
        this.products = products;
        notifyDataSetChanged();
    }

    /**
     * Вспомогательный класс элементов списка (продукты на складе)
     */
    class ProductListViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        TextView pcs;

        ProductListViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            pcs = itemView.findViewById(R.id.pcs);
            itemView.setOnClickListener(v -> {
                mClickListener.onItemClick(itemView, getAdapterPosition());
            });
        }

    }//eoc ProductListViewHolder

}//eoc ProductListAdapter
