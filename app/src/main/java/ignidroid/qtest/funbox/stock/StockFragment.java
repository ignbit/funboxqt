package ignidroid.qtest.funbox.stock;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import ignidroid.qtest.funbox.BaseFragment;
import ignidroid.qtest.funbox.R;
import ignidroid.qtest.funbox.model.Product;

/**
 * Склад
 */
public class StockFragment extends BaseFragment {

    RecyclerView list;
    ProductListAdapter adapter;
    private OnItemClickListener mClickListener;
    public interface OnItemClickListener {
        void onItemClick(View itemView, int position);
    }
    public void setOnItemClickListener(OnItemClickListener listener) {
        mClickListener = listener;
    }

    private OnAddNewListener addClickListener;
    public interface OnAddNewListener {
        void OnAddNew();
    }
    public void setOnAddNewListener(OnAddNewListener listener) {
        addClickListener = listener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.stock, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        prepareToolbar(null, null, R.menu.stock_list, false);
        list = view.findViewById(R.id.stock_list);
        adapter = new ProductListAdapter(getContext(), activity.getProducts());
        list.setAdapter(adapter);
        list.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter.setOnItemClickListener((itemView, position) -> {
            mClickListener.onItemClick(itemView, position);
        });
    }

    @Override
    protected void menuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_stock_add:
                addNewProduct();
                break;
        }
    }

    @Override
    public void update(ArrayList<Product> data) {
        adapter.notifyDataSetChanged();
    }

    private void addNewProduct() {
        addClickListener.OnAddNew();
    }

}//eoc