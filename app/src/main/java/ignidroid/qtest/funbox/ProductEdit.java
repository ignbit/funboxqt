package ignidroid.qtest.funbox;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import java.util.ArrayList;

import ignidroid.qtest.funbox.model.Product;

/**
 * Редактирование информации о товаре
 */
public class ProductEdit extends BaseFragment {

    public static final String ARG_INDEX_TO_EDIT = "ARG_INDEX_TO_EDIT";
    public static final int INDEX_NEW_PRODUCT = -1;
    private int index = INDEX_NEW_PRODUCT;
    private Product product;
    EditText name, price, pcs;
    private OnSavingChangesListener onSavingChangesListener = (index1, product1) -> {};
    public interface OnSavingChangesListener {
        /**
         * Сохранение информации о товаре после редактирования
         * @param index индекс измененного товара
         * @param product новые данные о товаре
         */
        void OnSave(int index, Product product);
    }
    public void setOnSavingChangesListener(OnSavingChangesListener listener) {
        onSavingChangesListener = listener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.product_edit, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        prepareToolbar(null,null,R.menu.product_edit, true);
        name = view.findViewById(R.id.name);
        price = view.findViewById(R.id.price);
        pcs = view.findViewById(R.id.stock);
        Bundle args = getArguments();
        if(args != null) {
            index = getArguments().getInt(ARG_INDEX_TO_EDIT, INDEX_NEW_PRODUCT);
        }
        if(index != INDEX_NEW_PRODUCT) {
            //product = activity.getProducts().get(index);
            Product tmp = activity.getProducts().get(index);
            //копируем информацию о товаре
            product = new Product(tmp.getName(), tmp.getPrice(), tmp.getStock());
            setProductView(product);
        } else {
            product = new Product();
        }
    }

    private void setProductView(Product product) {
        name.setText(product.getName());
        price.setText(String.valueOf(product.getPrice()));
        pcs.setText(String.valueOf(product.getStock()));
    }

    @Override
    protected void menuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_product_save:
                save();
                break;
        }
    }

    @Override
    public void update(ArrayList<Product> data) {
        if(index != INDEX_NEW_PRODUCT) {
            setProductView(data.get(index));
        }
    }

    /**
     * Сохранение данных после редактирования
     */
    private void save() {
        acceptChanges();
        onSavingChangesListener.OnSave(index, product); //событие сохранения результата
        activity.onBackPressed(); //открываем предыдущий экран
    }

    /**
     * Подвтерждение и проверка данных сохранения
     */
    private void acceptChanges() {
        String defaultName = getString(R.string.default_product_name);
        double defaultPrice = 0;
        int defaultStock = 0;
        String tmp = name.getText().toString();
        product.setName(tmp.isEmpty() ? defaultName : tmp);
        tmp = price.getText().toString();
        if(tmp.isEmpty()) {
            product.setPrice(defaultPrice);
        } else {
            product.setPrice(Double.valueOf(tmp));
        }
        tmp = pcs.getText().toString();
        if(tmp.isEmpty()) {
            product.setStock(defaultStock);
        } else {
            product.setStock(Integer.valueOf(tmp));
        }
    }

}//eoc
