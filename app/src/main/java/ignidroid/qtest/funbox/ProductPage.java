package ignidroid.qtest.funbox;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DecimalFormat;

import ignidroid.qtest.funbox.model.Product;

/**
 * Страница продукта (отображение и редактирование)
 */
public class ProductPage extends Fragment {

    public static final String ARG_PRODUCT = "ARG_PRODUCT";
    Product product;

    public ProductPage() {}

    public static ProductPage newInstance(Product product) {
        Bundle args = new Bundle();
        args.putParcelable(ARG_PRODUCT, product);
        ProductPage fragment = new ProductPage();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Bundle args = getArguments();
        if(args!=null) {
            product = args.getParcelable(ARG_PRODUCT);
        }
        return inflater.inflate(R.layout.product_info, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TextView name = view.findViewById(R.id.name);
        TextView price = view.findViewById(R.id.price);
        TextView stock = view.findViewById(R.id.stock);
        if(product != null) {
            name.setText(product.getName());
            DecimalFormat dec = new DecimalFormat(/*"#.##"*/);
            dec.setGroupingUsed(true);
            price.setText(getString(R.string.price_value,  dec.format(product.getPrice())));
            stock.setText(getString(R.string.stock_value, product.getStock()));
        }
    }

}//eoc
