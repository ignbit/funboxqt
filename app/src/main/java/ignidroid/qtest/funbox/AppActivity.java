package ignidroid.qtest.funbox;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.StringRes;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import ignidroid.qtest.funbox.model.Product;
import ignidroid.qtest.funbox.model.ProductSaver;
import ignidroid.qtest.funbox.stock.StockFragment;
import ignidroid.qtest.funbox.store.StoreFragment;

import static android.support.design.widget.Snackbar.LENGTH_SHORT;
import static ignidroid.qtest.funbox.ProductEdit.INDEX_NEW_PRODUCT;

/**
 * Основная активность приложения
 */
public class AppActivity extends AppCompatActivity {

    /**
     * Время покупки товара в мс
     */
    public static final int BUY_TIME = 3000;
    /**
     * Вермя сохранения информации о товаре в ми
     */
    public static final int SAVE_TIME = 5000;

    /**
     * список продуктов
     */
    private ArrayList<Product> products;
    private Toolbar toolbar; //верхнее меню приложения
    private PurchaseTask purchaseTask; //задача покупки товара
    private UpdateStockTask updateStockTask; //задача сохранения данных о товаре
    private ProductSaver productSaver;

    @Override
    protected void onStop() {
        if(purchaseTask != null) {
            purchaseTask.cancel(true);
        }
        if(updateStockTask != null) {
            updateStockTask.cancel(true);
        }
        super.onStop();
    }

    /**
     * Верхнее меню приложения
     */
    public Toolbar getToolbar() {
        return toolbar;
    }

    /**
     * Нижнее меню навигации
     */
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = item -> {
        switch (item.getItemId()) {
            case R.id.navigation_store: //магазин
                Bundle args = new Bundle();
                args.putParcelableArrayList(StoreFragment.ARG_PRODUCTS, products);
                setStoreFragment(args);
                return true;
            case R.id.navigation_stock: //склад
                setStockFragment(null);
                return true;
        }
        return false;
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app);
        toolbar = findViewById(R.id.toolbar);
        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        productSaver = new ProductSaver(getApplicationContext());
        products = productSaver.readProducts(); //загружаем данные о продуктах
        Bundle args = new Bundle();
        args.putParcelableArrayList(StoreFragment.ARG_PRODUCTS, products);
        setStoreFragment(args); //init default fragment
    }

    /**
     * Ссылка на список продуктов
     */
    public ArrayList<Product> getProducts() {
        return products;
    }

    /**
     * Установка фрагмента магазина
     * @param args аргументы
     */
    private void setStoreFragment(Bundle args) {
        StoreFragment store = new StoreFragment();
        store.setArguments(args);
        store.setOnBuyListener(position -> {
            showMessage(toolbar, R.string.purchase_start_check_stock);
            purchaseTask = new PurchaseTask();
            purchaseTask.execute(position, position);
        });
        changeFragment(store);
    }

    /**
     * Установка фрагмента склада
     * @param args аргументы
     */
    private void setStockFragment(Bundle args) {
        StockFragment stock = new StockFragment();
        stock.setArguments(args);
        stock.setOnItemClickListener((itemView, position) -> {
            //редактирование товара
            Bundle args1 = new Bundle();
            args1.putInt(ProductEdit.ARG_INDEX_TO_EDIT, position);
            setProductEditFragment(args1);
        });
        stock.setOnAddNewListener(() -> {
            //добавление нового товара
            Bundle args1 = new Bundle();
            args1.putInt(ProductEdit.ARG_INDEX_TO_EDIT, INDEX_NEW_PRODUCT);
            setProductEditFragment(args1);
        });
        changeFragment(stock);
    }

    /**
     * Установка фрагмента редактирования товара
     * @param args аргументы
     */
    private void setProductEditFragment(Bundle args) {
        ProductEdit editPage = new ProductEdit();
        editPage.setArguments(args);
        editPage.setOnSavingChangesListener((index, product) -> {
            if (index != ProductEdit.INDEX_NEW_PRODUCT) { //редактирование
                showMessage(toolbar, R.string.try_update_product_info);
                //запускаем процесс сохранения
                product.setId(index); //указываем индекс редактирования
                updateStockTask = new UpdateStockTask();
                updateStockTask.execute(product);
            } else { //добавление товара
                //добавляем в конец списка новый товар
                products.add(product);
                if(productSaver.writeProducts(products)) {
                    showMessage(toolbar, R.string.update_stock_ok);
                }
            }
        });
        changeFragment(editPage, true);
    }

    /**
     * Замена фрагмента в разметке
     * @param fr фрагмент главной активности
     */
    private void changeFragment(BaseFragment fr) {
        changeFragment(fr, false);
    }

    /**
     * Замена фрагмента в разметке
     * @param fr фрагмент главной активности
     * @param saveInStack сохранить в стеке переходов
     */
    private void changeFragment(BaseFragment fr, boolean saveInStack) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction tr = manager.beginTransaction();
        tr.replace(R.id.frame, fr);
        if(saveInStack) {
            //сохраняем транзакцию
            tr.addToBackStack("savedFragment");
        } else {
            //очищаем стек
            manager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
        tr.commit();
    }

    /**
     * Ссылка на текущий фрагмент активности
     */
    private BaseFragment getCurFragment() {
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        return (BaseFragment) fragments.get(fragments.size() - 1);
    }

    /**
     * Задача оплаты товара
     */
    private class PurchaseTask extends AsyncTask<Integer, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Integer... position) {
            SystemClock.sleep(BUY_TIME);
            Product product = products.get(position[0]);
            int stock = product.getStock();
            if(stock == 0) {
                return false;
            }
            product.setStock(product.getStock() - 1);
            productSaver.writeProducts(products);
            return true;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if(aBoolean) {
                getCurFragment().update(products); //обновим текущий фрагмент
                showMessage(toolbar, R.string.purchase_ok);
            } else {
                showMessage(toolbar, R.string.purchase_fail);
            }
        }
    }//eoc PurchaseTask

    /**
     * Задача обновления данных о товаре
     */
    private class UpdateStockTask extends AsyncTask<Product, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Product... products1) {
            SystemClock.sleep(SAVE_TIME);
            Product product = products1[0];
            products.set(product.getId(), product);
            return productSaver.writeProducts(products);
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if(aBoolean) {
                getCurFragment().update(products); //обновим текущий фрагмент
                showMessage(toolbar, R.string.update_stock_ok);
            } else {
                showMessage(toolbar, R.string.update_stock_fail);
            }
        }
    }//eoc UpdateStockTask

    /**
     * Всплывающее сообщение
     * @param view объект привязки разметки
     * @param message строковый ресурс с текстом сообщения
     */
    private static void showMessage(View view, @StringRes int message) {
        Snackbar snack;
        snack = Snackbar.make(view, message, LENGTH_SHORT);
        snack.setAction(R.string.close, v -> snack.dismiss());
        snack.show();
    }

}//eoc AppActivity
