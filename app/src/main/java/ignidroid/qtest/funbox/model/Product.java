package ignidroid.qtest.funbox.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Товар
 */
public class Product implements Parcelable {

    /**
     * Идентифиактор
     */
    private int id;
    /**
     * Название
     */
    private String name;
    /**
     * Цена
     */
    private double price;
    /**
     * Количество ("на складе")
     */
    private int stock;

    public Product() {}
    public Product(String name, double price, int stock) {
        this.name = name;
        this.price = price;
        this.stock = stock;
    }

    protected Product(Parcel in) {
        id = in.readInt();
        name = in.readString();
        price = in.readDouble();
        stock = in.readInt();
    }

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getStock() {
        return stock;
    }

    /**
     * Вналичие на складе
     * @return {@code true} - если на складе не менее 1-го товара, иначе - {@code false}
     */
    public boolean inStock() {
        return  stock > 0;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeDouble(price);
        dest.writeInt(stock);
    }

}//eoc
