package ignidroid.qtest.funbox.model;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public interface ReadWritableAdapter {

    ArrayList<Product> read(InputStream is) throws IOException;
    //public Product Parse();
    boolean write(BufferedWriter writer, ArrayList<Product> products) throws IOException;

}
