package ignidroid.qtest.funbox.model;

import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import ignidroid.qtest.funbox.R;

/**
 * Класс импорта/экспорта товаров
 */
public class ProductSaver {

    private static final String storage = "db";
    private Context context;
    /**
     * Адаптер импорта-экспорта
     */
    private ReadWritableAdapter adapter;

    /**
     * Адаптер для сохранения товаров
     * @param applicationContext контекст приложения
     */
    public ProductSaver(Context applicationContext) {
        context = applicationContext;
        adapter = new ProductCsvAdapter(); //текущий адаптер csv-формата
    }

    /**
     * Загрузка списка товаров из хранилища
     * @return список товаров
     */
    @NonNull
    public ArrayList<Product> readProducts() {
        File file = new File(context.getFilesDir(), storage);
        InputStream is;
        //проверим есть ли файл текущего хранилища
        if(!file.exists()) { // хранилище не существует, берем данные по умолчанию
            is = context.getResources().openRawResource(R.raw.data); //данные из raw/data.csv
            try {
                return new ProductCsvAdapter().read(is);
            } catch (IOException e) {
                Toast.makeText(context, R.string.storage_read_fail, Toast.LENGTH_SHORT).show();
                return new ArrayList<>();
            }
        } else  { //иначе парсим хранилище
            try {
                is = new FileInputStream(file);
            } catch (FileNotFoundException e) {
                Toast.makeText(context, R.string.storage_not_available, Toast.LENGTH_SHORT).show();
                return new ArrayList<>();
            }
        }
        try {
            return adapter.read(is);
        } catch (IOException e) {
            Toast.makeText(context, R.string.storage_read_fail, Toast.LENGTH_SHORT).show();
            return new ArrayList<>();
        }
    }

    /**
     * Добавление тестовых данных
     */
    @Deprecated
    private static void addTestData(ArrayList<Product> products) {
        products.add(new Product("Apple iPod touch 5 32Gb", 8888.99, 5));
        products.add(new Product("Nikon D3100 Kit", 13290, 0));
        products.add(new Product("Samsung Galaxy S Duos S7562", 7430, 2));
        products.add(new Product("Canon EOS 600D Kit", 15659, 4));
    }

    /**
     * Выгрузка товаров в хранилище
     * @param products список товаров
     * @return {@code true} в случае успешного сохранения, иначе - {@code false}
     */
    public boolean writeProducts(ArrayList<Product> products) {
        File file = new File(context.getFilesDir(), storage);
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(file,false));
            return adapter.write(writer, products);
        } catch (IOException e) {
            Toast.makeText(context, R.string.storage_write_fail, Toast.LENGTH_SHORT).show();
            return false;
        }
    }

}//eoc
