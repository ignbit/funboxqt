package ignidroid.qtest.funbox.model;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class ProductCsvAdapter implements ReadWritableAdapter {

    @Override
    public ArrayList<Product> read(InputStream is) throws IOException {
        ArrayList<Product> products = new ArrayList<>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        String line;
        while((line = reader.readLine()) != null) {
            Product product = parse(line);
            products.add(product);
        }
        reader.close();
        return products;
    }

    /**
     * Парсер csv формата
     * @param line строка данных
     * @return товар {@link Product}
     */
    private Product parse(String line) {
        Product product = new Product();
        String[] par = line.split(", ", 3);
        line = par[0];
        product.setName(line.substring(1, line.length() - 1));
        line = par[1];
        product.setPrice(Double.valueOf(line.substring(1, line.length() - 1)));
        line = par[2];
        product.setStock(Integer.valueOf(line.substring(1, line.length() - 1)));
        return product;
    }

    @Override
    public boolean write(BufferedWriter writer, ArrayList<Product> products) throws IOException {
        char QUOTE = '\"';
        String SEPARATOR = ", ";
        StringBuilder res = new StringBuilder();
        for (Product product: products) {
            res.append(QUOTE).append(product.getName()).append(QUOTE).append(SEPARATOR);
            res.append(QUOTE).append(product.getPrice()).append(QUOTE).append(SEPARATOR);
            res.append(QUOTE).append(product.getStock()).append(QUOTE);
            writer.write(res.toString());
            writer.newLine();
            res.setLength(0);
        }
        writer.close();
        return true;
    }

}//eoc
